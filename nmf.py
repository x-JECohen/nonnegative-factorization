# -*- coding: utf-8 -*-
"""
Created on Thur Jun 03 2021

@author: amarmore, cohenjer
"""

import numpy as np
import time

def nmf(data, rank, init = "random", U_0 = None, V_0 = None, n_iter_max=100, tol=1e-8,
           sparsity_coefficients = [None, None], fixed_modes = [], normalize = [False, False],
           verbose=False, return_costs=False):
    """
    ======================================
    Nonnegative Matrix Factorization (NMF)
    ======================================

    Factorization of a matrix M in two nonnegative matrices U and V,
    such that the product UV approximates M.
    If M is of size m*n, U and V are respectively of size m*r and r*n,
    r being the rank of the decomposition (parameter)
    Typically, this method is used as a dimensionality reduction technique,
    or for source separation.

    The objective function is:

        ||M - UV||_Fro^2
        + sparsity_coefficients[0] * (\sum\limits_{j = 0}^{r}||U[:,k]||_1)
        + sparsity_coefficients[1] * (\sum\limits_{j = 0}^{r}||V[k,:]||_1)

    With:

        ||A||_Fro^2 = \sum_{i,j} A_{ij}^2 (Frobenius norm)
        ||a||_1 = \sum_{i} abs(a_{i}) (Elementwise L1 norm)

    The objective function is minimized by fixing alternatively
    one of both factors U and V and optimizing on the other one,
    the problem being reduced to a Nonnegative Least Squares problem.

    More precisely, the chosen optimization algorithm is the HALS [1],
    which updates each factor columnwise, fixing every other columns.

    Parameters
    ----------
    data: nonnegative array
        The matrix M, which is factorized
    rank: integer
        The rank of the decomposition
    init: "random" |  "custom" |
        - If set to random:
            Initialize with random factors of the correct size.
            The randomization is the uniform distribution in [0,1),
            which is the default from numpy random.
        - If set to custom:
            U_0 and V_0 (see below) will be used for the initialization
        Default: random
    U_0: None or array of nonnegative floats
        A custom initialization of U, used only in "custom" init mode.
        Default: None
    V_0: None or array of nonnegative floats
        A custom initialization of V, used only in "custom" init mode.
        Default: None
    n_iter_max: integer
        The maximal number of iteration before stopping the algorithm
        Default: 100
    tol: float
        Threshold on the improvement in cost function value.
        Between two succesive iterations, if the difference between
        both cost function values is below this threshold, the algorithm stops.
        Default: 1e-8
    sparsity_coefficients: List of float (two)
        The sparsity coefficients on U and V respectively.
        If set to None, the algorithm is computed without sparsity
        Default: [None, None],
    fixed_modes: List of integers (between 0 and 2)
        Has to be set not to update a factor, 0 and 1 for U and V respectively
        Default: []
    normalize: List of boolean (two)
        Indicates whether the factors need to be normalized or not.
        The normalization is a l_2 normalization on each of the rank components
        (columnwise for U, linewise for V)
        Default: [False, False]
    verbose: boolean
        Indicates whether the algorithm prints the successive
        normalized cost function values or not
        Default: False
    return_costs: boolean
        Indicates whether the algorithm should return all normalized cost function
        values and computation time of each iteration or not
        Default: False

    Returns
    -------
    U, V: numpy arrays
        Factors of the NMF
    cost_fct_vals: list
        A list of the normalized cost function values, for every iteration of the algorithm.
    toc: list
        A list with accumulated time for every iterations

    Example
    -------
    >>> import numpy as np
    >>> from nn_fac import nmf
    >>> rank = 5
    >>> U_lines = 100
    >>> V_col = 125
    >>> U_0 = np.random.rand(U_lines, rank)
    >>> V_0 = np.random.rand(rank, V_col)
    >>> M = U_0@V_0
    >>> U, V = nmf.nmf(M, rank, init = "random", n_iter_max = 500, tol = 1e-8,
               sparsity_coefficients = [None, None], fixed_modes = [], normalize = [False, False],
               verbose=True, return_costs = False)

    References
    ----------
    [1]: N. Gillis and F. Glineur, Accelerated Multiplicative Updates and
    Hierarchical ALS Algorithms for Nonnegative Matrix Factorization,
    Neural Computation 24 (4): 1085-1105, 2012.

    [2]: C. Boutsidis and E. Gallopoulos. "SVD based
    initialization: A head start for nonnegative matrix factorization,"
    Pattern Recognition 41.4 (2008), pp. 1350{1362.

    [3]: B. Zupan et al. "Nimfa: A python library for nonnegative matrix
    factorization", Journal of Machine Learning Research 13.Mar (2012),
    pp. 849{853.
    """
    if init.lower() == "random":
        k, n = data.shape
        U_0 = np.random.rand(k, rank)
        V_0 = np.random.rand(rank, n)

    elif init.lower() == "custom":
        if U_0 is None or V_0 is None:
            raise Exception("Custom initialization, but one factor is set to 'None'")

    else:
        raise Exception('Initialization type not understood')

    return compute_nmf(data, rank, U_0, V_0, n_iter_max=n_iter_max, tol=tol,
                   sparsity_coefficients = sparsity_coefficients, fixed_modes = fixed_modes, normalize = normalize,
                   verbose=verbose, return_costs=return_costs)

# Author : Jeremy Cohen, modified by Axel Marmoret
def compute_nmf(data, rank, U_in, V_in, n_iter_max=100, tol=1e-8,
           sparsity_coefficients = [None, None], fixed_modes = [], normalize = [False, False],
           verbose=False, return_costs=False):
    """
    Computation of a Nonnegative matrix factorization via
    hierarchical alternating least squares (HALS) [1],
    with U_in and V_in as initialization.

    Parameters
    ----------
    data: nonnegative array
        The matrix M, which is factorized, of size m*n
    rank: integer
        The rank of the decomposition
    U_in: array of floats
        Initial U factor, of size m*r
    V_in: array of floats
        Initial V factor, of size r*n
    n_iter_max: integer
        The maximal number of iteration before stopping the algorithm
        Default: 100
    tol: float
        Threshold on the improvement in cost function value.
        Between two iterations, if the difference between
        both cost function values is below this threshold, the algorithm stops.
        Default: 1e-8
    sparsity_coefficients: List of float (two)
        The sparsity coefficients on U and V respectively.
        If set to None, the algorithm is computed without sparsity
        Default: [None, None],
    fixed_modes: List of integers (between 0 and 2)
        Has to be set not to update a factor, 0 and 1 for U and V respectively
        Default: []
    normalize: List of boolean (two)
        Indicates whether the factors need to be normalized or not.
        The normalization is a l_2 normalization on each of the rank components
        (columnwise for U, linewise for V)
        Default: [False, False]
    verbose: boolean
        Indicates whether the algorithm prints the successive
        normalized cost function values or not
        Default: False
    return_costs: boolean
        Indicates whether the algorithm should return all normalized cost function
        values and computation time of each iteration or not
        Default: False

    Returns
    -------
    U, V: numpy arrays
        Factors of the NMF
    cost_fct_vals: list
        A list of the normalized cost function values, for every iteration of the algorithm.
    toc: list
        A list with accumulated time at each iterations

    References
    ----------
    [1]: N. Gillis and F. Glineur, Accelerated Multiplicative Updates and
    Hierarchical ALS Algorithms for Nonnegative Matrix Factorization,
    Neural Computation 24 (4): 1085-1105, 2012.
    """
    # initialisation
    U = U_in.copy()
    V = V_in.copy()
    cost_fct_vals = []
    norm_data = norm(data)
    tic = time.time()
    toc = []

    if sparsity_coefficients == None:
        sparsity_coefficients = [None, None]
    if fixed_modes == None:
        fixed_modes = []
    if normalize == None or normalize == False:
        normalize = [False, False]

    for iteration in range(n_iter_max):

        # One pass of least squares on each updated mode
        U, V, cost = one_nmf_step(data, rank, U, V, norm_data,
                                       sparsity_coefficients, fixed_modes, normalize)

        toc.append(time.time() - tic)

        cost_fct_vals.append(cost)

        if verbose:
            if iteration == 0:
                print('Normalized cost function value={}'.format(cost))
            else:
                if cost_fct_vals[-2] - cost_fct_vals[-1] > 0:
                    print('Normalized cost function value={}, variation={}.'.format(
                            cost_fct_vals[-1], cost_fct_vals[-2] - cost_fct_vals[-1]))
                else:
                    # print in red when the reconstruction error is negative (shouldn't happen)
                    print('\033[91m' + 'Normalized cost function value={}, variation={}.'.format(
                            cost_fct_vals[-1], cost_fct_vals[-2] - cost_fct_vals[-1]) + '\033[0m')

        if iteration > 0 and abs(cost_fct_vals[-2] - cost_fct_vals[-1]) < tol:
            # Stop condition: relative error between last two iterations < tol
            if verbose:
                print('Converged in {} iterations.'.format(iteration))
            break

    if return_costs:
        return np.array(U), np.array(V), cost_fct_vals, toc
    else:
        return np.array(U), np.array(V)


def one_nmf_step(data, rank, U_in, V_in, norm_data,
                 sparsity_coefficients, fixed_modes, normalize):
    """
    One pass of updates for each factor in NMF
    Update the factors by solving a nonnegative least squares problem per mode.

    Parameters
    ----------
    data: nonnegative array
        The matrix M, which is factorized, of size m*n
    rank: integer
        The rank of the decomposition
    U_in: array of floats
        Initial U factor, of size m*r
    V_in: array of floats
        Initial V factor, of size r*n
    norm_data: float
        The Frobenius norm of the input matrix (data)
    sparsity_coefficients: List of float (two)
        The sparsity coefficients on U and V respectively.
        If set to None, the algorithm is computed without sparsity
        Default: [None, None],
    fixed_modes: List of integers (between 0 and 2)
        Has to be set not to update a factor, 0 and 1 for U and V respectively
        Default: []
    normalize: List of boolean (two)
        A boolean whereas the factors need to be normalized.
        The normalization is a l_2 normalization on each of the rank components
        (columnwise for U, linewise for V)
        Default: [False, False]

    Returns
    -------
    U, V: numpy arrays
        Factors of the NMF
    cost_fct_val:
        The value of the cost function at this step,
        normalized by the squared norm of the original matrix.
    """

    if len(sparsity_coefficients) != 2:
        raise ValueError("NMF needs 2 sparsity coefficients to be performed")

    # Copy
    U = U_in.copy()
    V = V_in.copy()

    if 0 not in fixed_modes:
        # U update

        # Set timer for acceleration in hals_nnls_acc
        tic = time.time()

        # Computing cross products
        VVt = np.dot(V,np.transpose(V))
        VMt = np.dot(V,np.transpose(data))

        # End timer for acceleration in hals_nnls_acc
        timer = time.time() - tic

        # Compute HALS/NNLS resolution
        U = np.transpose(hals_nnls_acc(VMt, VVt, np.transpose(U_in), maxiter=100, atime=timer, alpha=0.5, delta=0.01,
                                            sparsity_coefficient = sparsity_coefficients[0], normalize = normalize[0], nonzero = False)[0])

    if 1 not in fixed_modes:
        # V update

        # Set timer for acceleration in hals_nnls_acc
        tic = time.time()

        # Computing cross products
        UtU = np.dot(np.transpose(U),U)
        UtM = np.dot(np.transpose(U),data)

        # End timer for acceleration in hals_nnls_acc
        timer = time.time() - tic

        # Compute HALS/NNLS resolution
        V = hals_nnls_acc(UtM, UtU, V_in, maxiter=100, atime=timer, alpha=0.5, delta=0.01,
                               sparsity_coefficient = sparsity_coefficients[1], normalize = normalize[1], nonzero = False)[0]

    sparsity_coefficients = np.where(np.array(sparsity_coefficients) == None, 0, sparsity_coefficients)

    cost = norm(data-np.dot(U,V)) ** 2 + 2 * (sparsity_coefficients[0] * np.sum(U.flatten())  + sparsity_coefficients[1] * np.sum(V.flatten()) )

    cost = cost/(norm_data**2)

    return U, V, cost

def hals_nnls_acc(UtM, UtU, in_V, maxiter=500, atime=None, alpha=0.5, delta=0.01,
                      sparsity_coefficient = None, normalize = False, nonzero = False):
    ## Author : Axel Marmoret, based on Jeremy Cohen version's of Nicolas Gillis Matlab's code for HALS

        """
        =================================
        Non Negative Least Squares (NNLS)
        =================================

        Computes an approximate solution of a nonnegative least
        squares problem (NNLS) with an exact block-coordinate descent scheme.
        M is m by n, U is m by r, V is r by n.
        All matrices are nonnegative componentwise.

        The NNLS unconstrained problem, as defined in [1], solve the following problem:

                min_{V >= 0} ||M-UV||_F^2

        The matrix V is updated linewise.

        The update rule of the k-th line of V (V[k,:]) for this resolution is:

                V[k,:]_(j+1) = V[k,:]_(j) + (UtM[k,:] - UtU[k,:] V_(j))/UtU[k,k]

        with j the update iteration.

        This problem can also be defined by adding a sparsity coefficient,
        enhancing sparsity in the solution [2]. The problem thus becomes:

                min_{V >= 0} ||M-UV||_F^2 + 2*sparsity_coefficient*(\sum\limits_{j = 0}^{r}||V[k,:]||_1)

        NB: 2*sp for uniformization in the derivative

        In this sparse version, the update rule for V[k,:] becomes:

                V[k,:]_(j+1) = V[k,:]_(j) + (UtM[k,:] - UtU[k,:] V_(j) - sparsity_coefficient)/UtU[k,k]

        This algorithm is defined in [1], as an accelerated version of the HALS algorithm.

        It features two accelerations: an early stop stopping criterion, and a
        complexity averaging between precomputations and loops, so as to use large
        precomputations several times.

        This function is made for being used repetively inside an
        outer-loop alternating algorithm, for instance for computing nonnegative
        matrix Factorization or tensor factorization.

        Parameters
        ----------
        UtM: r-by-n array
            Pre-computed product of the transposed of U and M, used in the update rule
        UtU: r-by-r array
            Pre-computed product of the transposed of U and U, used in the update rule
        in_V: r-by-n initialization matrix (mutable)
            Initialized V array
            By default, is initialized with one non-zero entry per column
            corresponding to the closest column of U of the corresponding column of M. Empty entry is not supported.
        maxiter: Postivie integer
            Upper bound on the number of iterations
            Default: 500
        atime: Positive float
            Time taken to do the precomputations UtU and UtM
            Default: None
        alpha: Positive float
            Ratio between outer computations and inner loops, typically set to 0.5 or 1.
            Default: 0.5
        delta : float in [0,1]
            early stop criterion, while err_k > delta*err_0. Set small for
            almost exact nnls solution, or larger (e.g. 1e-2) for inner loops
            of a PARAFAC computation.
            Default: 0.01
        sparsity_coefficient: float or None
            The coefficient controling the sparisty level in the objective function.
            If set to None, the problem is solved unconstrained.
            Default: None
        normalize: boolean
            True in order to normalize each of the k-th line of V after the update
            False not to update them
            Default: False
        nonzero: boolean
            True if the lines of the V matrix can't be zero,
            False if they can be zero
            Default: False

        Returns
        -------
        V: array
            a r-by-n nonnegative matrix \approx argmin_{V >= 0} ||M-UV||_F^2
        eps: float
            number of loops authorized by the error stop criterion
        cnt: integer
            final number of update iteration performed
        rho: float
            number of loops authorized by the time stop criterion

        References
        ----------
        [1]: N. Gillis and F. Glineur, Accelerated Multiplicative Updates and
        Hierarchical ALS Algorithms for Nonnegative Matrix Factorization,
        Neural Computation 24 (4): 1085-1105, 2012.

        [2] J. Eggert, and E. Korner. "Sparse coding and NMF."
        2004 IEEE International Joint Conference on Neural Networks
        (IEEE Cat. No. 04CH37541). Vol. 4. IEEE, 2004.

        """

        r, n = np.shape(UtM)
        V = in_V.copy()

        rho = 100000
        eps0 = 0
        cnt = 1
        eps = 1

        # Start timer
        tic = time.time()
        while eps >= delta * eps0 and cnt <= 1+alpha*rho and cnt <= maxiter:
            nodelta = 0
            for k in range(r):

                if UtU[k,k] != 0:

                    if sparsity_coefficient != None: # Using the sparsifying objective function
                        deltaV = np.maximum((UtM[k,:] - UtU[k,:]@V - sparsity_coefficient * np.ones(n)) / UtU[k,k], -V[k,:])
                        V[k,:] = V[k,:] + deltaV

                    else:
                        deltaV = np.maximum((UtM[k,:]- UtU[k,:]@V) / UtU[k,k],-V[k,:]) # Element wise maximum -> good idea ?
                        V[k,:] = V[k,:] + deltaV

                    nodelta = nodelta + np.dot(deltaV, np.transpose(deltaV))

                    # Safety procedure, if columns aren't allow to be zero
                    if nonzero and (V[k,:] == 0).all() :
                        V[k,:] = 1e-16*np.max(V)

                elif nonzero:
                    raise ValueError("Column " + str(k) + " of U is zero with nonzero condition")

                if normalize:
                    normV = norm(V[k,:])
                    if normV != 0:
                        V[k,:] /= normV
                    else:
                        sqrt_n = 1/n ** (1/2)
                        V[k,:] = [sqrt_n for i in range(n)]

            if cnt == 1:
                eps0 = nodelta
                # End timer for one iteration
                btime = max(time.time() - tic, 10e-7) # Avoid division by 0

                if atime:  # atime is provided
                    # Number of loops authorized
                    rho = atime/btime
            eps = nodelta
            cnt += 1

        return V, eps, cnt, rho


def norm(A):
    """
    A custom implementation of norm to avoid np.linalg, only for 2-way arrays
    """
    return np.sqrt(np.sum(A.flatten()**2))
